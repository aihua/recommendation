#encoding:utf-8
from pyspark import *
#import numpy
import jpype
from pyspark.ml.recommendation import *
from pyspark.sql import *
from pyspark.sql.SQLContext import *
from pyspark.sql.functions  import *
from pyspark.mllib.recommendation import ALS
jvmPath=jpype.getDefaultJVMPath()
jpype.startJVM(jvmPath)
java_prop=jpype.JClass("java.util.Properties")
new_prop=java_prop()
new_prop.setProperty("user","root")
new_prop.setProperty("password","yuzhou123")
mysql_url="jdbc:mysql://10.117.215.139:3306/spark_database"
def conn_mysql(param):
    return sqlContext.read.jdbc(url=mysql_url,table=param,properties=new_prop)

#处理推荐特征相关
collect_conn=conn_mysql("user_collect_card").collect()
praise_conn=conn_mysql("user_praise_card").collect()
friend_conn=conn_mysql("friend_ship")
follow_conn=conn_mysql("user_follow")

#处理已读问题
dislike_conn=conn_mysql("user_dislike_card").collect()

#处理推荐排序和长度相关
user_conn=conn_mysql("dp_user")
card_conn=conn_mysql("user_card")
#all_user_numbers=user_conn.count()
#推荐的用户列表
user_df=user_conn.select("uid").distinct()
#推荐的物品列表
item_df=card_conn.select("id").distinct()

#将间接数据转化为直接数据
follow_praise=follow_conn.join(praise_conn,follow_conn.follow_uid==praise_conn.uid,"inner").select(follow_conn.uid,praise_conn.card_id)

follow_collect=follow_conn.join(collect_conn,follow_conn.follow_uid==collect_conn.uid,"inner").select(follow_conn.uid,collect_conn.card_id)

follow_collect_praise=follow_praise.unionAll(follow_collect).distinct()

friend_praise=friend_conn.join(praise_conn,friend_conn.friend_uid==praise_conn.uid,"inner").select(friend_conn.uid,praise_conn.card_id)

friend_collect=friend_conn.join(collect_conn,friend_conn.friend_uid==collect_conn.uid,"inner").select(friend_conn.uid,collect_conn.card_id)

friend_collect_praise=friend_praise.unionAll(friend_collect).distinct()

#转化评分
praise_rdd=praise_conn.map(lambda p:[p.uid,p.card_id,5.0])
collect_rdd=collect_conn.map(lambda p:[p.uid,p.card_id,5.0])
friend_rdd=friend_collect_praise.map(lambda p:[p.uid,p.card_id,4.0])
follow_rdd=follow_collect_praise.map(lambda p:[p.uid,p.card_id,4.0])

#user_card_rdd=praise_rdd.union(collect_rdd).union(friend_rdd).union(follow_rdd).distinct()
#优化:数量大的在前，数量小的在后，待测数据库的大小

#最终的转化后带排序的数据
user_card_rdd=praise_rdd.union(collect_rdd).union(follow_rdd).union(friend_rdd).distinct().collect()
#优化:数量大的在前，数量小的在后，待测数据库的大小

praise_rdd_test=praise_conn.map(lambda p:[p.uid,p.card_id])
collect_rdd_test=collect_conn.map(lambda p:[p.uid,p.card_id])
friend_rdd_test=friend_collect_praise.map(lambda p:[p.uid,p.card_id])
follow_rdd_test=follow_collect_praise.map(lambda p:[p.uid,p.card_id])
user_card_rdd_test=praise_rdd_test.union(collect_rdd_test).union(follow_rdd_test).union(friend_rdd_test).distinct().collect()

list_user_100=[]
list_item_100=[]
#训练模型，得到参数
def train(data_rdd):
    user_item_model=ALS.train(data_rdd)
    return user_item_model

#传入的参数为待训练的数据集和为每个用户的推荐物品数;返回[[]...,[uer_i_item1,user_i_item2,...],...]
#其中user_id_list为用户id的列表
def data_recommen_sort(finish_model,item_num,user_id_df,user_card_rdd):
    #user_item_model=ALS.train(data_rdd,5)
    #list_user_100=[]
    #计算现有的用户总数
    user_numbers=user_id_df.count()
    #给出用户的id列表
    user_id_list=user_id_df.map(lambda x:x.uid).take(user_numbers)
    #为每个用户进行推荐
    dict_i_100={}
    
    for u in user_id_list:
        dict_i_100.clear()
        
        #为用户u推荐的物品列表
        list_i_100=finish_model.recommendProducts(u,item_num)
        #挑出该用户已经浏览收藏或不喜欢的物品
        df_u_collect=collect_conn.fliter(collect_conn.uid==list_i_100[0].user)
        df_u_praise=praise_conn.fliter(praise_conn.uid==list_i_100[0].user)
        df_u_dislike=dislike_conn.fliter(dislike_conn.uid==list_i_100[0].user)
        
        #为了提高查询效率将各个值先保存
        collect_count=df_u_collect.count()
        if(collect_count!=0):
            collect_list=df_u_collect.map(lambda x:x.card_id).take(collect_count)
            
        praise_count=df_u_praise.count()
        if(praise_count!=0):
            praise_list=df_u_praise.map(lambda x:x.card_id).take(praise_count)

        dislike_count=df_u_dislike.count()
        if(dislike_count!=0):
            dislike_list=df_u_dislike.map(lambda x:x.card_id).take(dislike_count)
        
        #k为推荐的物品ID
        k=0
        list_i_new=[]
        for i in list_i_100:
            k=i
            if(collect_count!=0):
                for j in collect_list:
                    if(j==i):
                        k=0
            if(k!=0 and praise_count!=0):
                for j in praise_list:
                    if(j==i):
                        k=0
            if(k!=0 and dislike_count!=0):
                for j in dislike_list:
                    if(j==i):
                        k=0
        if(k!=0):
            #推荐的item排序
            list_i_new=list_i_new+[k]
        dict_i_100[u]=list_i_new
        list_user_100.append(dict_i_100)
    return list_user_100
    
#相连用户的挖掘，减轻数据稀疏问题;评论量大了以后可以减小权重;传入一个训练好的模型
def relative_user(finish_model,user_num,item_id_df):
    item_numbers=item_id_df.count()
    item_id_list=item_id_df.map(lambda x:x.id).take(item_numbers)
    for i in item_id_list:
        list_i_100=finish_model.recommendUsers(i,user_num)
        list_i_100=[i]+list_i_100
        list_item_100.append(list_i_100)
    return list_item_100
    
    
#传入测试数据集与待训练数据集，显示测试的方差均值 ;离线测试程式，上线的时候不执行  
def test_train_data(test_rdd,train_rdd):
    #作为测试集合用
    
    user_card_data_test=sqlContext.createDataFrame(test_rdd,["user","item"]).collect()
    #分为训练集合和测试集;此处全体既当训练，又当测试
    #user_card_rdd_train,user_card_rdd_test=user_card_rdd.randomSplit([0.8,0.2])
    #优化：先聚合减少数据计算和存储
    #praise_DataFrame=sqlContext.createDataFrame(praise_rdd)
    #collect_DataFrame=sqlContext.createDataFrame(collect_rdd)
    #friend_DataFrame=sqlContext.createDataFrame(friend_rdd)
    #follow_DataFrame=sqlContext.createDataFrame(follow_rdd)
    #all_user_card_data=praise_DataFrame.unionAll(collect_DataFrame).unionAll(praise_DataFrame).unionAll(follow_DataFrame).unionAll(friend_DataFrame).distinct()

    user_card_data=sqlContext.createDataFrame(train_rdd,["user","item","rating"]).collect()
    #注意创建的时候要符合als的输入格式，也即最后一个list要加入。

    als=ALS(rank=10,maxIter=10,regParam=0.1)
    user_card_model=als.fit(user_card_data)
    #优化算法用到的特征参数个数
    user_card_model.itemFactors
    user_card_model.userFactors

    #编写测试程式时：是会取一部分作为测试集。
    #test_user_card=sqlContext.createDataFrame([],["user","item"])   其中测试追加list必须注意
    #predictions=sorted(model.transform(user_card_data_test).collect(),key=lambda x:x[0])
    user_card_pre=user_card_model.transform(user_card_data_test).collect()

    #得到验证集合
    user_card_pre_test=user_card_pre.join(user_card_data,["user","item"])

    #user_card_sub=user_card_pre_test.map(user_card_pre_test.col("rating")-user_card_pre_test.col("prediction")
    user_card_sub=user_card_pre_test["rating"]-user_card_pre_test["prediction"]
    user_card_err=user_card_pre_test.select(mean(user_card_sub*user_card_sub))
    #user_card_err=user_card_sub_new.map(lambda x:x.user_card_sub)
    user_card_err.show()

def main():
    #离线测试程式
    test_train_data(user_card_rdd_test,user_card_rdd)
    #训练模型
    user_card_model=train(user_card_rdd)
    #此作为稀疏矩阵挖掘因子用
    item_relative_list=relative_user(user_card_model,2,item_df)
    #获取推荐排序链表,此链表放入redis中
    recommon_list=data_recommen_sort(user_card_model,100,user_df)
    
 
if __name__ == "__main__":
    main()